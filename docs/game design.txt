EMOJIS:


 U+1F5FB 🗻
 U+1F3D4 🏔 MOUNTAIN

Volcano:
U+1F30B


U+1F3D5 🏕 CAMPING


U+1F3DB 	🏛 BUILDING

U+2B50 	⭐
U+2697 	⚗

U+1F6CF 	🛏  BED
U+26B0 	⚰ CASKET

star cinder
star well
cloak of ashes
amulet of xulang
living fire
zavid's compass
tenwick's walking stick



oubliette

paragon


The Archlocks:

Narz/Nargril
Bodias
Vezial
Caladar
Mog
Desilla
Karnivex

eras:

primordial/primal? era before death

era of death / great dying?

the dawning

the wind age

reign of the pillars

the rending

age of silence

war of the locks




HIGH TIER FURNISHINGS
dimensional library
deeper dungeon? name?
dream woods

everflame


staff of the dryad
fazbit's workshop
fazbit's fixations
the black spindle
robes of tharandul

sirith tal

ellister's boots of bounding

sindel - star cinder?

epiphany

treatise
text
folio
lorebook
compendium
magister's manual

grove/glade?

new sections:
familiars,
alchemy,
crafting

baffling
befuddlement

upgrades:

cairn ( druid)
obelisk

astral projection
crocodile tears
- as crocodiles do not cry, they are notoriously difficult to come by.

weapons:
Shillelagh

Types/Associations:

Earth: Defense/Hp
Water: Dodge/Stamina
Air: Speed/Dodge?
Fire: damage
Tempus: Speed


ongoing/continuous

master names?
quizlbach


beds + stamina/rest speed

homes /sanctums / domiciles / base

obscura




alcove?
corner?

cabin

crypt

mausoleum

library

archive

pocket dimension

plane



Dungeons:

misty grove
bright forest
dark forest
gloomy forest
verdant hills

the werry


spell/potion order:

minor
lesser
(normal)
greater
major?
superior

So I have little interest in counting out the motes of dust you take for a lineage,
and piling them up like shivering dunes of sand,
to be scattered away with the faintest breeze.
A hundred of your generations are but a passing day to me,
and now you come, asking to be forgiven at evening for the mistakes you made this very afternoon.”